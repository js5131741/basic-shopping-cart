import React from "react";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import mensOuterwear from "./Data/mensOuterwear";
import ladiesOuterwear from "./Data/ladiesOuterwear";
import mensTshirt from "./Data/mensTshirt";
import ladiesTshirt from "./Data/ladiesTshirt";
import NavigationButtons from "./Components/NavigationButtons";
import Header from "./Components/Header";
import ClothesCard from "./Components/ClothesCard";
import ShoppingCartPage from "./Components/ShoppingCartPage";

export const CartContext = React.createContext();
export default function App() {
  const [cartItems, setCartItems] = React.useState([]);

  return (
    <div>
      <CartContext.Provider value={{ cartItems, setCartItems }}>
        <Header />
        <Routes>
          <Route
            path="/"
            element={
              <div>
                <NavigationButtons />
                <img
                  src="./images/mens_outerwear.jpg"
                  className="clothtype-image"
                  alt="mens-outerwear here"
                />
                <ClothesCard data={mensOuterwear} heading={"Men's Outerwear"} />
              </div>
            }
          ></Route>
          <Route
            path="/ladies-outerwear"
            element={
              <div>
                <NavigationButtons />
                <img
                  src="./images/ladies_outerwear.jpg"
                  className="clothtype-image"
                  alt="ladies-outerwear here"
                />
                <ClothesCard
                  data={ladiesOuterwear}
                  heading={"Ladies Outerwear"}
                />
              </div>
            }
          ></Route>
          <Route
            path="/mens-tshirts"
            element={
              <div>
                <NavigationButtons />
                <img
                  src="./images/ladies_tshirts.jpg"
                  className="clothtype-image"
                  alt="ladies-tshirts here"
                />
                <ClothesCard data={mensTshirt} heading={"Men's T-shirts"} />
              </div>
            }
          ></Route>
          <Route
            path="/ladies-tshirts"
            element={
              <div>
                <NavigationButtons />
                <img
                  src="./images/mens_tshirts.jpg"
                  className="clothtype-image"
                  alt="mens-tshirts here"
                />
                <ClothesCard data={ladiesTshirt} heading={"Ladies T-shirts"} />
              </div>
            }
          ></Route>
          <Route
            path="/shopping-cart"
            element={
              <div>
                <ShoppingCartPage />
              </div>
            }
          ></Route>
        </Routes>
      </CartContext.Provider>
    </div>
  );
}
