import React from "react";
import { useNavigate } from "react-router-dom";

export default function NavigationButtons() {
  const navigate = useNavigate();

  return (
    <div className="navigation-buttons">
      <button className="nav-button" onClick={() => navigate("/")}>
        Men's Outerwear
      </button>
      <button
        className="nav-button"
        onClick={() => navigate("/ladies-outerwear")}
      >
        Ladies Outerwear
      </button>
      <button className="nav-button" onClick={() => navigate("/mens-tshirts")}>
        Men's T-shirts
      </button>
      <button
        className="nav-button"
        onClick={() => navigate("/ladies-tshirts")}
      >
        Ladies T-shirts
      </button>
    </div>
  );
}
