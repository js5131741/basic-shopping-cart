import React from "react";
import { CartContext } from "../App";

export default function ShoppingCartPage() {
  const cartItems = React.useContext(CartContext).cartItems;

  const cartItemsObj = cartItems.reduce((acc, item) => {
    if (!acc.hasOwnProperty(item.title)) {
      acc[item.title] = { ...item, count: 1 };
    } else {
      acc[item.title].count += 1;
    }
    return acc;
  }, {});

  console.log(cartItemsObj);

  return (
    <div>
      <div className="cart-page-header">
        <h1>Your shopping cart</h1>
        <p>{`${cartItems.length} items in cart`}</p>
      </div>
      {Object.keys(cartItemsObj).length === 0 ? (
        <div className="no-items">
          <h3>No items in cart. Try Adding items.</h3>
        </div>
      ) : (
        <div className="cloth-cards">
          {Object.keys(cartItemsObj).map((title) => {
            const item = cartItemsObj[title];
            return (
              <div key={title} className="card-content">
                <img src={item.imageUrl} alt="clothes" />
                <div className="card-content">
                  <h4>{item.title}</h4>
                  <p>${item.price}</p>
                  <p>{`Count: ${item.count}`}</p>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
