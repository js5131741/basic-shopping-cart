import React from "react";
import { useNavigate } from "react-router-dom";
import { CartContext } from "../App";

export default function Header() {
  const navigate = useNavigate();
  const cartContext = React.useContext(CartContext);
  console.log(cartContext.cartItems);
  return (
    <header>
      <h3 onClick={() => navigate("/")}>SHOP</h3>
      <div
        className="shopping-cart-image"
        onClick={() => navigate("/shopping-cart")}
      >
        <img src="./images/shopping_cart.svg" alt="shopping-cart here" />
        <span className="counter">{cartContext.cartItems.length}</span>
      </div>
    </header>
  );
}
