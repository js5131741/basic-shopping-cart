import React from "react";
import { CartContext } from "../App";

export default function ClothesCard({ data, heading }) {
  const setCartItems = React.useContext(CartContext).setCartItems;

  const handleClick = React.useCallback(
    (productData) => {
      setCartItems((prevCartItems) => [...prevCartItems, productData]);
    },
    [setCartItems]
  );

  return (
    <div>
      <div className="clothtype-title">
        <h4>{heading}</h4>
        <p>({data.length} items)</p>
      </div>

      <div className="cloth-cards">
        {data.map((item) => (
          <div key={item.id}>
            <div className="card-content">
              <img
                src={`https://shop.polymer-project.org/esm-bundled/${item.largeImage}`}
                alt="item-here"
              />
              <h4>{item.title}</h4>
              <p>${item.price.toFixed(2)}</p>
              <button
                className="add-to-cart-button"
                onClick={() =>
                  handleClick({
                    title: item.title,
                    imageUrl: `https://shop.polymer-project.org/esm-bundled/${item.largeImage}`,
                    price: item.price.toFixed(2),
                  })
                }
              >
                Add To Cart
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
