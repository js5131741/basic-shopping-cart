## Getting Started

- Clone the repository.

```
git clone https://gitlab.com/js5131741/basic-shopping-cart.git
```

- Cd into the directory and install the dependencies.

```
npm install
```

- Run the following command to view the project.

```
npm start
```
